import pandas as pd

def translateSymbol(variable):
    '''Function that returns the name of a variable as a string.'''
    
    StringVariable = [k for k,v in locals().items() if v==variable][0]
    
    return StringVariable

def loadDatabase(url):
    '''Function that loads the database from a .csv'''
    
    df = pd.read_csv(url, sep=';')
    
    return df

def giveExplanation(df, VariableName):
    '''Gives the explanation of the demanded symbol.'''
    
    Expl = df.loc[df['VariableName']==VariableName]['Explanation'][0]
    
    return Expl
