INPUT=$1
OUTPUT=$2

pandoc -f markdown+pandoc_title_block -V papersize:a4 -V documentclass:report $INPUT.md --pdf-engine=xelatex -o $OUTPUT.pdf
pandoc -f markdown+pandoc_title_block -V papersize:a4 -V documentclass:report $INPUT.md -o $OUTPUT.docx

