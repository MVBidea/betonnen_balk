# Materiaaltabellen

In deze folder worden alle materiaaltabellen weergegeven die gebruikt worden in de code.

Voorlopig zijn deze nog allemaal in .csv. Later zullen deze omgevormd moeten worden naar tabellen in een database. Bij de folder ../Webscraping vind je de code terug hoe men aan deze tabellen komt.