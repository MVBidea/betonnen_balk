import pandas as pd

def Load_Wapenings(path='../idea18/MateriaalTabellen/WapeningStavenInfo.csv'):
    #path = '../idea18/MateriaalTabellen/WapeningStavenInfo.csv'
    df = pd.read_csv(path)
    df1 = df[['Name','Dia']]
    df1.columns = ['label','value']
    return df1.to_dict('records')

def Load_beton(path='../idea18/MateriaalTabellen/betonklasse.csv'):
    #path = '../idea18/MateriaalTabellen/WapeningStavenInfo.csv'
    df = pd.read_csv(path, sep=';')
    df1 = df[['betonklasse','f_ck']]
    df1.columns = ['label','value']
    return df1.to_dict('records')

def Load_beton(path='../idea18/MateriaalTabellen/betonklasse.csv'):
    #path = '../idea18/MateriaalTabellen/WapeningStavenInfo.csv'
    df = pd.read_csv(path, sep=';')
    df1 = df[['betonklasse','f_ck']]
    df1.columns = ['label','value']
    return df1.to_dict('records')

def Load_staal(path='../idea18/MateriaalTabellen/StaalKwaliteitBeton.csv'):
    #path = '../idea18/MateriaalTabellen/WapeningStavenInfo.csv'
    df = pd.read_csv(path, sep=';')
    df1 = df[['Staalkwaliteit','f_yk']]
    df1.columns = ['label','value']
    return df1.to_dict('records')