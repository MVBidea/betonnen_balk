import dash
import dash_core_components as dcc
import dash_html_components as html
import base64

from app import app

def tegel(path, url):
    image_filename = path # replace with your own image
    encoded_image = base64.b64encode(open(image_filename, 'rb').read())
    img_app = html.A([
        html.Img(
            src='data:image/png;base64,{}'.format(encoded_image.decode()),
            style={
                'height': '200px',
                'width': '200px',
                'float' : 'left',
                'padding' : 10})],
        href=url, target="_blank")
    return img_app

img_app1 = tegel(path='images/app1_image.png', url='http://127.0.0.1:8051/')
img_app2 = tegel(path='images/app2_image.png', url='/apps/app2')

layout = html.Div([img_app1 ,img_app2])