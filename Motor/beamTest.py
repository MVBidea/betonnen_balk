import plotly.graph_objs as go
from plotly.offline import plot

class beamTest:
    
    shapes = []
    
    def set_height(self, height):
        '''Define the height in m.'''
        self.height=height
        #print('De hoogte van de balk is %s m.'%(self.height))
        
    def set_width(self, width):
        '''Define the width in m.'''
        self.width=width
        #print('De breedte van de balk is %s m.'%(self.width))
        
    def set_origin(self, x, y):
        '''Define in meters'''
        self.ox=x
        self.oy=y
        #print('De oorsprong van de balk is coördinaat (%s,%s).'%(self.ox,self.oy))

    def set_cover(self, a):
        '''Define in meters'''
        self.cover=a
        #print('De betondekking heeft een dikte van %s m.'%(self.cover))

    def set_beugel(self, a):
        '''Define in meters (diameter)'''
        self.beugel=a
        #print('De beugel heeft een diameter van %s m.'%(self.cover))
        
    def set_onderwapening(self, a, count):
        '''Define the diameter in meters and the amount of rebar'''
        l = self.width-2*self.cover-2*self.beugel
        spacing = (l-a)/(count-1)
        
        x0 = self.ox + self.cover + self.beugel + a/2
        y0 = self.oy + self.cover + self.beugel + a/2
        
        matrix = []
        for x in range(count):
            x_w = x0 + spacing*x
            y_w = y0
            
            wap = {'x_w':x_w,
                  'y_w':y_w,
                  'dia':a}
            matrix.append(wap)
        
        self.onderwapening = matrix
        self.oppervlakte_onderwapening = round(count*3.14159265359*(a**2)/4,6)
        self.zwaartepunt_onderwapening = self.height - self.cover - self.beugel - a/2
        #print(self.onderwapening)
    
    def set_betonklasse(self, f_ck_v):
        '''waarde in N/mm2'''
        self.f_cd = 0.85*f_ck_v/1.5
        
    def set_Design_Moment(self, M_d):
        '''waarde in kN/m'''
        self.M_d = M_d
        
    def calc_mu_d(self):
        self.mu_d = round((self.M_d*1000)/(self.width*(self.zwaartepunt_onderwapening*1000)**2*self.f_cd),3)
        
    def generatecontour(self):
        '''Generates the contour of the beam'''
        x0 = self.ox
        y0 = self.oy
        
        x1 = x0 + self.width
        y1 = y0 + self.height
        
        contour = {
            'type': 'rect',
            'x0': x0,
            'y0': y0,
            'x1': x1,
            'y1': y1,
            'fillcolor': 'rgba(160, 160, 160, 1.0)',
            'line': {
                'color': 'rgba(128, 0, 128, 1)',
            }}
        
        return contour
    
    def generatecover(self):
        '''Generates the cover of the beam'''
        x0 = self.ox + self.cover
        y0 = self.oy + self.cover
        
        x1 = x0 + self.width - 2*self.cover
        y1 = y0 + self.height - 2*self.cover
        
        cover = {
            'type': 'rect',
            'x0': x0,
            'y0': y0,
            'x1': x1,
            'y1': y1,
            'fillcolor': 'rgba(0,0,255,0.7)',
            'line': {
                'color': 'rgba(0,0,255,0.7)',
            }}
        
        return cover
    
    def generatebeugel(self):
        '''Generates the cover of the beam'''
        x0 = self.ox + self.cover + self.beugel
        y0 = self.oy + self.cover + self.beugel
        
        x1 = x0 + self.width - 2*self.cover - 2*self.beugel
        y1 = y0 + self.height - 2*self.cover - 2*self.beugel
        
        beugel = {
            'type': 'rect',
            'x0': x0,
            'y0': y0,
            'x1': x1,
            'y1': y1,
            'fillcolor': 'rgba(255,255,255,1)',
            'line': {
                'color': 'rgba(255,255,255,1)',
            }}
        
        return beugel
    
    def generateonderwapening(self, rebar):
        '''Generates the bottomrebar'''
        x0 = rebar['x_w'] - rebar['dia']/2
        y0 = rebar['y_w'] - rebar['dia']/2
        
        x1 = rebar['x_w'] + rebar['dia']/2
        y1 = rebar['y_w'] + rebar['dia']/2
        
        rebar ={
            'type': 'circle',
            'xref': 'x',
            'yref': 'y',
            'fillcolor': 'rgba(0,0,255,0.7)',
            'x0': x0,
            'y0': y0,
            'x1': x1,
            'y1': y1,
            'line': {
                'color': 'rgba(0,0,255,0.7)',
            }}
        
        return rebar
    
    def generate_rectangle_shape(self):
        
        self.shapes.append(self.generatecontour())
        self.shapes.append(self.generatecover())
        self.shapes.append(self.generatebeugel())
        
        for x in self.onderwapening:
            self.shapes.append(self.generateonderwapening(x))

        #print(self.shapes)
    
    def plot_beam(self):
        
        a=(self.oy+self.height+0.1-(self.oy-0.1)-self.width)/2

        trace0 = go.Scatter(
            x=[self.ox+(self.width/2), self.ox+self.width+0.03],
            y=[self.oy-0.02, self.oy+(self.height/2)],
            text=[str(self.width)+'m', 
                  str(self.height)+'m'],
            mode='text',
        )
        data = [trace0]
        layout = {
            'xaxis': {
                'range': [self.ox-a, self.ox+self.width+a],
                'showgrid':False,
                'zeroline':False,
                'showline':False,
                'ticks':'',
                'showticklabels':False
            },
            'yaxis': {
                'range': [self.oy-0.1, self.oy+self.height+0.1],
                'showgrid':False,
                'zeroline':False,
                'showline':False,
                'ticks':'',
                'showticklabels':False
            },
            'shapes': self.shapes,
#           'height':600,
#           'width':600
        }

        config={'showLink': False, 'displayModeBar': False}

        fig = {
            'data': data,
            'layout': layout,
        }
        #plot(fig, filename='shapes-rectangle.html', config=config)
        
        return fig, config