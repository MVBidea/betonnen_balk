from beamTest import beamTest
#from Elasticformulas import get_boundary_e, get_formulas, getMu_d
beamA = beamTest()

beamA.set_height(0.300)
beamA.set_width(0.100)
beamA.set_origin(0,0)
beamA.set_cover(0.005)
beamA.set_beugel(0.008)
beamA.set_onderwapening(0.006, 5)
#beamA.plot_beam()
beamA.generate_rectangle_shape()
#beamA.plot_beam()

fig, config = beamA.plot_beam()

def headerblocks(name):
    string = '''
    *naming.*
    '''.replace('naming', name)
    
    block = html.Div([
        html.Div([
            dcc.Markdown(string.replace('  ', ''))
        ],
            className='five columns',
            style={'display': 'table-cell',
                   'text-align': 'right',
                   'font-size': '25px',
                   'vertical-align': 'middle',
                   'border-width':'1px',
                   'border-bottom-width':'1px',
                   'border-bottom-color':'LightGrey',
                   'border-bottom-style':'solid'})
    ],
        className='twelve columns')  
    return block

def parameterblocks_Input(name, id_input, type_input, placeholder_input, value_input=''):
    
    block = html.Div([
        html.Div([
            dcc.Markdown(name+": ")
        ],
            className='five columns',
            style={'display': 'table-cell',
                   'font-size': '20px',
                   'text-align': 'right',
                   'vertical-align': 'middle'}),
        html.Div([
            dcc.Input(
                placeholder=placeholder_input,
                id=id_input,
                type=type_input,
                value=str(value_input),
                min=0,
                className='twelve columns',
                style={'height':'35px',
                   'font-size': '14.5px'})],
                className='seven columns')
    ],
        className='twelve columns')
        
    return block

def openingblocks():
    block = dcc.Markdown('''opening'''.replace('  ', ''),
                         className='twelve columns',
                         containerProps={'style': {'color':'white'}}) 
    return block

def parameterblocks_Dropdown(name):
    
    block = html.Div([
        html.Div([
            dcc.Markdown(name+": ")
        ],
            className='five columns',
            style={'display': 'table-cell',
                   'font-size': '20px',
                   'text-align': 'right',
                   'vertical-align': 'middle'}),
        html.Div([
            dcc.Dropdown(
                options=[
                    {'label': 'New York City', 'value': 'NYC'},
                    {'label': 'Montréal', 'value': 'MTL'},
                    {'label': 'San Francisco', 'value': 'SF'}],
                value='MTL')],
                className='seven columns')
            
    ],
        className='twelve columns')
        
    return block

# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import numpy as np
import forecastio

import datetime
import base64
import json
import pandas as pd
import plotly
import dash_table_experiments as dt
import io

import base64

app = dash.Dash()
app.title='Een betonnen balk.'

app.css.append_css({
    'external_url': (
        'https://cdn.rawgit.com/chriddyp/0247653a7c52feb4c48437e1c1837f75'
        '/raw/a68333b876edaf62df2efa7bac0e9b3613258851/dash.css'
    )
})

app.layout = html.Div([
    dcc.Markdown('''
    ## Een betonnen balk.
    '''.replace('  ', ''),
                 className='twelve columns',
                 containerProps={'style': {'text-align': 'center',
                                          'height': '7vh',
                                           #'padding':' 10px',
                                           'border-width':'1px',
                                           'border-bottom-width':'1px',
                                           'border-bottom-color':'LightGrey',
                                           'border-bottom-style':'solid'}}),
    html.Div([
        html.Div([
            headerblocks('Geometrie'),
            parameterblocks_Input(name='Hoogte',id_input='Hoogte',type_input='number',placeholder_input='Geef een waarde in mm', value_input=240),
            parameterblocks_Input(name='Breedte',id_input='Breedte',type_input='number',placeholder_input='Geef een waarde in mm', value_input=140),
            openingblocks(),
            headerblocks('Beton'),
            parameterblocks_Dropdown('Betonkwaliteit'),
            parameterblocks_Input(name='Betondekking',id_input='Betondekking',type_input='number',placeholder_input='Geef een waarde in mm', value_input=25),
            openingblocks(),
            headerblocks('Wapening'),
            parameterblocks_Dropdown('Staalkwaliteit'),
            parameterblocks_Dropdown('Dia. langswap.'),
            parameterblocks_Dropdown('Dia. beugel'),
            parameterblocks_Input(name='Aantal staven',id_input='Aantal_staven',type_input='number',placeholder_input='Aantal staven', value_input=4),
            openingblocks()
        ],
            className='four columns',
            style={'overflow-y': 'auto',
                  'height': '91vh',
                  'padding-top':'20px',
                   'border-width':'1px',
                   'border-right-width':'1px',
                   'border-right-color':'LightGrey',
                   'border-right-style':'solid'}),
        html.Div([
            dcc.Graph(
                id='betonbalk',
                figure=fig, 
                config=config,
                className='twelve columns',style={'height': '85vh'}
    )
        ],
            className='eight columns',
            style={'overflow-y': 'auto',
                  'height': '91vh',
                  'width': '91vh',
                  'padding-top':'1px'})
    ],
        className='row')
])

@app.callback(
    Output('betonbalk', 'figure'),
    [Input('Hoogte', 'value'),
    Input('Breedte', 'value'),
    Input('Betondekking', 'value'),
    Input('Aantal_staven', 'value')])
def update_graph(hoogte, breedte, betondekking, aantal_staven):
    beamA = beamTest()
    beamA.shapes =[]
    beamA.set_height(int(hoogte)/1000)
    beamA.set_width(int(breedte)/1000)
    beamA.set_origin(0,0)
    beamA.set_cover(int(betondekking)/1000)
    beamA.set_beugel(0.008)
    beamA.set_onderwapening(0.006, int(aantal_staven))
    beamA.generate_rectangle_shape()
    
    fig, config = beamA.plot_beam()
    
    #print(value)

    return fig

if __name__ == '__main__':
    app.run_server(host='192.168.0.230', port='8050')    
