# Git commands

Dit geeft de status van alle files weer, lokaal en globaal.
```
git status
```
***
Voegt alle bestanden toe aan de commit om door te geven naar de globale repository.
```
git add *
```
***
Geeft de commit een reden en voegt deze toe aan het uiteindelijke verzenden naar de globale repository
```
git commit -m 'Reden van de commit'
```
***
Verzend de commit naar de globale repository en voegt deze toe aan een specifieke branch. In dit geval de master.
```
git push --set-upstream origin master
```