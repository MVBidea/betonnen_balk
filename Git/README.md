# Documentatie Git

Voor het beter begrijpen van Git zou ik volgende documentatie even doornemen:

- [Tutorial bitbucket](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud): Diagonaal doornemen volstaat


![Bitbucket cloud](Learn_git_with_bitbucket_cloud.png)

- In de sourcecode vind je ook nog wat regematig gebruikte git commando's voor Unix systemen.

- [Tutorial meerdere accounts op 1 repository](https://code.tutsplus.com/tutorials/quick-tip-how-to-work-with-github-and-multiple-accounts--net-22574)