% IDEA: Een idee rond een betonnen balk 
% 13 Juni 2018

# Algemeen doel & planning
Doel is het creëren van een 'pipeline' voor een simpele berekening als een gewapende betonnen balk op 2 steunpunten. Hiervoor is een modulaire aanpak van het probleem noodzakelijk. Men kan vier grote doelen onderscheiden in de modules:

1. Input-software
    - Eindige elementenpakket
    - Eigen geschreven software ter berekening van ${ M }_{ d }$ 
    - CAD software
    - Webscraping
    - ...
2. Verwerking en opslag
    - Motor/vertaler
    - API's van de verschillende interfaces aanspreken
    - MateriaalTabellen
    - ...
3. Output-software
    - CAD software
    - Excel
    - Rapportage in .pdf, .docx, ..
    - Dashboards
    - ...
4. Opmaak/documentering & overzicht
    - Git: versiecontrole van de software
    - Markdown: Documentatie van de software.
    - ...

Deze vierde stap is in de eerste fase (prototypering) niet zo belangrijk. Naarmate er getest wordt met meer dan 1 partij wordt dit noodzakelijk. Aan de hand van het overzicht van deze modules kunnen er deeltaken opgemaakt worden. Niet alle modules zijn nodig voor een pipeline te creëren. In de volgende paragraaf wordt dit meer toegelicht naar de keuzes die gemaakt zullen worden.

***

## Opdeling in deeltaken
De volgende modules worden in de app onder de loep genomen.

### Output
Voor de output stel ik voor dat we eerst het dashboard opstellen.
De CAD en rapportage lijken me iets voor het einde. Heel belangrijk is hier eerst te bepalen wat we willen zien.

1. FreeCAD
    - Connectie leggen met behulp van externe python
    - Tekenen van objecten(beton en staal)
    - Plotten van de balk
2. Rapportage
    - Denksessie over wat er gerapporteerd moet worden
    - Markdown
3. Dashboard
    - Dash van Plotly
    - Controlepanel maken voor de output van bestaande objecten

### Input

1. Elastische berekening
    - Algoritme van de berekening neerschrijven
    - Omvormen van formules naar SymPy
    - Optimalisatie-algoritme maken
    - Wapening en beton bepalen
2. Wapeningsgeometrie bepalen
    - Plaats van wapening
    - Dekking
    - Algoritme van geometrie neerschrijven

### Verwerking en opslag
De verwerking komt best als laatste. Is de lijm van heel het verhaal!
De structuur krijgt normaal wel vorm als alle modules gemaakt zijn.

1. Opslag/tabellen
    - Wegschrijven naar .csv met Pandas
2. Verwerking
    - Structuur van motor bepalen
    - Functies en klasses bepalen
3. API's
    - Functies van alle aangestuurde programma's maken

### Opmaak en documentering
Na elke creatie van een module zou alles goed gedocumenteerd worden.

1. GIT
    - Bitbucket openen voor versie-controle van de code
2. Markdown
    - Docstrings opstellen voor de verschillende gecreëerde functies
    - Automatische generatie van de pythoncode naar een website met MkDocs
3. Unit-testing
    - Een plan maken voor unit-testing. Zelf nog nooit gedaan, maar dit zou wel een goede methode zijn.
    
***

### Planning
Hieronder een overzicht van planning. Ultieme deadline van het project zonder tegenslagen (Dit is een voorwaardelijke datum):

> **30 september 2018**

  * Één grote brainstormsessie.

> *24 juni 2018*

> Inhoud: *Nadenken over voornamelijke structuur motor, praktische afspraken, uitleg GIT & vormgeven document.* 

> Rapport: *Dit document*

  * Elastische berekening.

> *Deadline 7 juli 2018*

> *Interne deadlines opstellen 24 Juni 2018*

> Inhoud: *Nadenken over IO, Pipeline van formules opstellen in Jupyter & vertalen naar code* 

> Rapport: *Taakverdeling & interne deadlines opstellen*

  * Volgende module.

> *Deadline *

> *Interne deadlines opstellen *

> Inhoud: *...* 

> Rapport: *...*



