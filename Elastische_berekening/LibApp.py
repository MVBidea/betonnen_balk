import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_auth
from dash.dependencies import Input, Output, State

def headerblocks(name):
    '''dit maakt een headerblock aan.'''
    string = '''
    *naming.*
    '''.replace('naming', name)
    
    block = html.Div([
        html.Div([
            dcc.Markdown(string.replace('  ', ''))
        ],
            className='five columns',
            style={'display': 'table-cell',
                   'text-align': 'right',
                   'font-size': '25px',
                   'vertical-align': 'middle',
                   'border-width':'1px',
                   'border-bottom-width':'1px',
                   'border-bottom-color':'LightGrey',
                   'border-bottom-style':'solid'})
    ],
        className='twelve columns')  
    return block

def parameterblocks_Input(name, id_input, type_input, placeholder_input, value_input='', unit_input='unit'):
    
    block = html.Div([
        html.Div([
            dcc.Markdown(name+": ")
        ],
            className='five columns',
            style={'display': 'table-cell',
                   'font-size': '20px',
                   'text-align': 'right',
                   'vertical-align': 'middle'}),
        html.Div([
            dcc.Input(
                placeholder=placeholder_input,
                id=id_input,
                type=type_input,
                value=str(value_input),
                min=0,
                className='twelve columns',
                style={'height':'35px',
                   'font-size': '14.5px'})],
                className='five columns'),
        html.Div([
            dcc.Markdown(unit_input)
        ],
            className='one column',
            style={'display': 'table-cell',
                   'font-size': '20px',
                   'text-align': 'left',
                   'vertical-align': 'middle'})
    ],
        className='twelve columns')
        
    return block

def openingblocks():
    block = dcc.Markdown('''opening'''.replace('  ', ''),
                         className='twelve columns',
                         containerProps={'style': {'color':'white'}}) 
    return block

def parameterblocks_Dropdown(name, id_input, options_value, value_value, unit_input='unit'):
    
    block = html.Div([
        html.Div([
            dcc.Markdown(name+": ")
        ],
            className='five columns',
            style={'display': 'table-cell',
                   'font-size': '20px',
                   'text-align': 'right',
                   'vertical-align': 'middle'}),
        html.Div([
            dcc.Dropdown(
                options=options_value,
                value=value_value,
                id=id_input)],
                className='five columns'),
        html.Div([
            dcc.Markdown(unit_input)
        ],
            className='one column',
            style={'display': 'table-cell',
                   'font-size': '20px',
                   'text-align': 'left',
                   'vertical-align': 'middle'})
            
    ],
        className='twelve columns')
        
    return block

def layoutOfApp(parameterblock, graph, title):
    return html.Div([
    dcc.Markdown('''
    ## *naming*.
    '''.replace('*naming*', title).replace('  ',''),
                 className='twelve columns',
                 containerProps={'style': {'text-align': 'center',
                                          'height': '7vh',
                                           #'padding':' 10px',
                                           'border-width':'1px',
                                           'border-bottom-width':'1px',
                                           'border-bottom-color':'LightGrey',
                                           'border-bottom-style':'solid'}}),
    html.Button('Save', id='save', style={'position': "absolute", 'top': '10', 'right': '10'}),
    html.Button('Load', id='load', style={'position': "absolute", 'top': '10', 'right': '115'}),
    html.Button('New', id='new', style={'position': "absolute", 'top': '10', 'right': '220'}),
    html.Div([
        html.Div(
            parameterblock,
            className='four columns',
            style={'overflow-y': 'auto',
                  'height': '91vh',
                  'padding':'20px',
                   'border-width':'1px',
                   'border-right-width':'1px',
                   'border-right-color':'LightGrey',
                   'border-right-style':'solid'},
            id='parameters'),
        html.Div(
            graph,
            className='eight columns',
            style={'overflow-y': 'auto',
                  'height': '91vh',
                  'width': '91vh',
                  'padding-top':'1px'})
    ],
        className='row')
],id='kantzijde')
