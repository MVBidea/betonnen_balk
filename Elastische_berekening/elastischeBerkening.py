from sympy import symbols
from sympy import solve, Eq, integrate, Piecewise
from sympy import init_printing
from sympy import latex
import plotly.graph_objs as go
import pandas as pd
init_printing(latex_printer=lambda *args, **kwargs: latex(*args, mul_symbol='dot', **kwargs))

e_c, e_c2, e_cu2, n, psi, f_ck, delta_G, sigma_c, f_cd = symbols('epsilon_c epsilon_c2 epsilon_cu2 n psi f_ck delta_G sigma_c f_cd')

def get_boundary_e(f_ck_v):
    e_c, e_c2, e_cu2, n, psi, f_ck, delta_G = symbols('epsilon_c epsilon_c2 epsilon_cu2 n psi f_ck delta_G')
    Eq4 = Eq(e_c2,Piecewise((2, f_ck<50), (2.0 + 0.085*(f_ck - 50)**0.53, f_ck>=50)))
    Eq5 = Eq(e_cu2,Piecewise((3.5, f_ck<50), (2.6 + 35*((90 - f_ck)/100)**4, f_ck>=50))) 
    Eq6 = Eq(n,Piecewise((2, f_ck<50), (1.4 + 23.4*((90 - f_ck)/100)**4, f_ck>=50))) 
    e_c2_value = float(solve(Eq4.subs(f_ck, f_ck_v), e_c2)[0]/1000)
    e_cu2_value = float(solve(Eq5.subs(f_ck, f_ck_v), e_cu2)[0]/1000)
    n_value = float(solve(Eq6.subs(f_ck, f_ck_v), n)[0])
    
    return e_c2_value, e_cu2_value, n_value

def get_formulas(e_c2_value, n_value, e_ud_value):
    xi, e_c, e_c2, e_cu2, e_ud, n, psi, f_ck, delta_G = symbols('xi epsilon_c epsilon_c2 epsilon_cu2 epsilon_ud n psi f_ck delta_G')
    Eq1 = Eq(psi, (1-(1-(e_c/e_c2))**n))
    Eq3 = integrate(solve(Eq1, psi)[0],e_c)
    Eq4 = Eq(psi, 1)
    Eq5 = integrate(solve(Eq4, psi)[0],e_c)
    
    Data=({e_c2:e_c2_value, n:n_value, e_c:e_c})
    Data1=({e_c2:e_c2_value, n:n_value, e_c:0})
    a1 = (Eq3.subs(Data)-Eq3.subs(Data1))/e_c
    
    Data=({e_c2:e_c2_value, n:n_value, e_c:e_c2_value})
    Data1=({e_c2:e_c2_value, n:n_value, e_c:0})
    Data2=({e_c:e_c2_value})
    Data3=({e_c:e_c})
    b1 = ((Eq3.subs(Data)-Eq3.subs(Data1))+(Eq5.subs(Data3)-Eq5.subs(Data2)))/(e_c)
    
    p_eq = Eq(psi, Piecewise((a1, e_c<=e_c2_value),(b1 , e_c>e_c2_value)))
    
    Eq10 = Eq(psi, (1-(1-(e_c/e_c2))**n)*e_c)
    Eq30 = integrate(solve(Eq10, psi)[0],e_c)
    Eq40 = Eq(psi, 1*e_c)
    Eq50 = integrate(solve(Eq40, psi)[0],e_c)
    
    Data=({e_c2:e_c2_value, n:n_value, e_c:e_c})
    Data1=({e_c2:e_c2_value, n:n_value, e_c:0})
    a = 1-((Eq30.subs(Data)-Eq30.subs(Data1))/(a1*e_c**2))
    
    Data=({e_c2:e_c2_value, n:n_value, e_c:e_c2_value})
    Data1=({e_c2:e_c2_value, n:n_value, e_c:0})
    Data2=({e_c:e_c2_value})
    Data3=({e_c:e_c})
    b = 1-(((Eq30.subs(Data)-Eq30.subs(Data1))+(Eq50.subs(Data3)-Eq50.subs(Data2)))/(b1*e_c**2))
    
    d_G_eq = Eq(delta_G, Piecewise((a, e_c<=e_c2_value),(b , e_c>e_c2_value)))
    
    xi_eq = Eq(xi, e_c/(e_c+e_ud)).subs({e_ud:e_ud_value})
    
    return p_eq, d_G_eq, xi_eq

def getMu_d(e_c_v, e_ud_v, p_eq, d_G_eq):
    xi, e_c, e_c2, e_cu2, e_ud, n, psi, f_ck, delta_G, mu_d = symbols('xi epsilon_c epsilon_c2 epsilon_cu2 epsilon_ud n psi f_ck delta_G mu_d')
    Eq90 = Eq(xi, e_c/(e_c+e_ud))
    xi_value = solve(Eq90.subs({e_c:e_c_v, e_ud:e_ud_v}))[0]
    delta_G_value = solve(d_G_eq.subs({e_c:e_c_v}))[0]
    psi_value = solve(p_eq.subs({e_c:e_c_v}))[0]
    Eq99 = Eq(mu_d, psi*xi*(1-delta_G*xi))
    mu_d_value = solve(Eq99.subs({psi:psi_value, delta_G:delta_G_value, xi:xi_value}))[0]
    return mu_d_value, psi_value, delta_G_value, xi_value

def all(e_ud_v, f_ck_v, e_c_v):
    
    a, b, c = get_boundary_e(f_ck_v)
    d, e = get_formulas(a,c)
    optimal_mu_d, optimal_psi, optimal_delta_G, optimal_xi = getMu_d(e_c_v, e_ud_v, d, e)
    print(e_c_v)
    return optimal_mu_d, optimal_psi, optimal_delta_G, optimal_xi


def allform(e_ud_v, f_ck_v, e_c_v1):
    e_c_v1 = symbols('epsilon_c_v1')
    a, b, c = get_boundary_e(f_ck_v)
    d, e = get_formulas(a,c)
    return getMu_d(e_c_v1, e_ud_v, d, e)





def psi_rek_df(e_ud_value, f_ck_value):
    e_ud_v = e_ud_value
    f_ck_v = f_ck_value
    a, b, c = get_boundary_e(f_ck_v)
    d, e, f = get_formulas(a,c, e_ud_v)
    
    Eq4 = Eq(sigma_c,Piecewise((f_cd*(1-(1-(e_c/e_c2))**n), e_c<e_c2), (f_cd, e_c>=e_c2)))
    Eq5 = Eq4.subs({e_c2:a, n:c, f_cd:(0.85*f_ck_v/1.5)})

    df = pd.DataFrame(pd.RangeIndex(1,round(b*100000),1))/100000
    df.columns = ['e_d']
    df['psi'] = df.e_d.apply(lambda x: float(solve(d.subs({e_c:x}))[0]))
    df['sigma'] = df.e_d.apply(lambda x: float(solve(Eq5.subs({e_c:x}))[0]))
    df['delta_G'] = df.e_d.apply(lambda x: float(solve(e.subs({e_c:x}))[0]))
    df['xi'] = df.e_d.apply(lambda x: float(solve(f.subs({e_c:x}))[0]))    
    return df

def plot_graph_psi_rek(df, ab):
    data=[
            go.Scatter(
                x=df['e_d'],
                y=df['psi'],
                name='psi (-)',
                mode='lines',
                marker=dict(
                    color='rgba(171, 197, 215, 1)')),
            go.Scatter(
                x=df['e_d'],
                y=df['sigma'],
                name='sigma_c',
                yaxis='y2',
                mode='lines',
                marker=dict(
                    color='rgba(0, 197, 215, 1)'))]

    layout = go.Layout(
            title='Spanning_rek/psi: '+ab,
            showlegend=True,
            yaxis=dict(
                title='psi',
                titlefont=dict(
                    family='RALEWAY THIN',
                    size=15,
                    color='#636466'
                )),
            yaxis2=dict(
                title='sigma',
                anchor='x',
                overlaying='y',
                side='right'
            ),
            xaxis=dict(
                title='rek (-)',
                titlefont=dict(
                    family='RALEWAY THIN',
                    size=15,
                    color='#636466'
                )),
            font=dict(
                family='RALEWAY THIN',
                size=15,
                color='#636466'),
            legend=go.Legend(
                x=-0.1,
                y=1.2),
            margin=go.Margin(
                l=40,
                r=40,
                t=40,
                b=70))
    
    fig = {
            'data': data,
            'layout': layout,
        }
    
    config={'showLink': False, 'displayModeBar': False}
    
    return fig, config