import FreeCAD as App
import FreeCADGui
import FreeCAD
import Part


class Beam:
    def __init__(self, obj):
        '''"App height, width and length of the beam" '''
        obj.addProperty("App::PropertyLength","Length","Beam","Length of the beam").Length=2000
        obj.addProperty("App::PropertyLength","Width","Beam","Width of the beam").Width=140
        obj.addProperty("App::PropertyLength","Height","Beam", "Height of the beam").Height=300
        obj.addProperty("App::PropertyVector","p1","Beam","origin")
        obj.Proxy = self

    def execute(self, fp):
        '''"Print a short message when doing a recomputation, this method is mandatory" '''
            x1 = 0
        x2 = 2400
        y1 = 0
        y2 = 300
        z = 140
        spacing = 100

        p1 = FreeCAD.Vector(x1, y1, 0)
        p2 = FreeCAD.Vector(x2, y1, 0)
        p3 = FreeCAD.Vector(x2, y2, 0)
        p4 = FreeCAD.Vector(x1, y2, 0)
        
        fp.Shape = Part.makeWire([p1,p2,p3,p4],closed=True)

        
        
a=FreeCAD.ActiveDocument.addObject("Part::FeaturePython","Line")
Line(a)
a.ViewObject.Proxy=0 # just set it to something different from None (this assignment is needed to run an internal notification)
FreeCAD.ActiveDocument.recompute() 
