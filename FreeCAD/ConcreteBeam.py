import FreeCAD,Draft

x1 = 0
x2 = 2400
y1 = 0
y2 = 300
z = 140
spacing = 100

p1 = FreeCAD.Vector(x1, y1, 0)
p2 = FreeCAD.Vector(x2, y1, 0)
p3 = FreeCAD.Vector(x2, y2, 0)
p4 = FreeCAD.Vector(x1, y2, 0)
Draft.makeWire([p1,p2,p3,p4],closed=True)


p1 = FreeCAD.Vector((x2+spacing), y1, 0)
p2 = FreeCAD.Vector((x2+spacing)+z, y1, 0)
p3 = FreeCAD.Vector((x2+spacing)+z, y2, 0)
p4 = FreeCAD.Vector((x2+spacing), y2, 0)
Draft.makeWire([p1,p2,p3,p4],closed=True)

p1 = FreeCAD.Vector(x1, y1, 0)
p2 = FreeCAD.Vector(x2 ,y1, 0)
p3 = FreeCAD.Vector(x1, (y1-200), 0)
Draft.makeDimension(p1,p2,p3)

Gui.SendMsgToActiveView("ViewFit")
